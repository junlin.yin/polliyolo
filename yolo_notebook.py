# Récupération des données

# In[ ]:

from matplotlib import pyplot as plt
import numpy as np

import PIL
from PIL import Image
import cv2
import glob, os, sys
import json as js
import time
import copy
from tensorflow.keras.utils import to_categorical
from scipy.special import expit, softmax
from tensorflow.keras.layers import Dense, Conv2D, MaxPooling2D, Flatten, Reshape, Dropout, Input
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras import regularizers
from tensorflow.keras.utils import Sequence
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint
import tensorflow as tf

# __________________________________________________________________________________________________________________________________

# In[ ]:
IMAGE_SIZE = 448 # Dimension des images en entrée du réseau
CELL_PER_DIM = 7 # Nombre de cellules en largeur et en hauteur
BOX_PER_CELL = 1 # Nombre d'objets par cellule
CLASS_LABELS = ['bee', 'beetle', 'butterfly', 'fly', 'other pollinator']
NB_CLASSES = len(CLASS_LABELS) # Nombre de classes du problème

# __________________________________________________________________________________________________________________________________
# In[ ]:
def padding_resize(img, shape_img):
   
    max_dim = max(shape_img)
    img_padding = Image.new('RGB', (max_dim, max_dim), (0, 0, 0))
    rapport = [shape_img[0]/max_dim , shape_img[1]/max_dim]
    img_padding.paste(img, (0, 0))

    # Redimensionner les images pour qu'elles aient la même taille
    # img_pad_resize = cv2.resize(img_padding, (IMAGE_SIZE, IMAGE_SIZE), 0, 0)
    img_pad_resize = img_padding.resize((IMAGE_SIZE, IMAGE_SIZE))

    return img_pad_resize, rapport

# image_path = "C:\\Users\\juyin\\.darwin\\datasets\\ue-inrae-toulouse\\pollinator\\images\\WSCT0358.JPG"
# image = Image.open(image_path)
# shape_img = np.shape(image)
# killing_me = padding_resize(image, shape_img)

# __________________________________________________________________________________________________________________________________

# In[6]:
# def lire_txts (folder_txt : str, imgs):
#     # il faut distinquer la lecture de fichier json et txt
#     # l'enregistrement dans txt est [[classe, x, y , w, h]]
#     lenth = len(imgs)
#     labels = np.zeros(lenth)
#     labels = np.ndarray.tolist(labels)
    
#     for file in glob.glob(folder_txt + "*.txt"):
#         annot_1img = []
#         with open(file , 'r') as file_txt:
#             ligne = file_txt.readline()
#             name_file = file.split("\\")[-1].rsplit(".",1)[0]
#             print(name_file)
#             while ligne != "":
#                 classe, x, y, h, w = ligne.split(" ")
#                 h, w, x, y = float(h), float(w), float(x), float(y)
#                 classe = int(classe)
#                 # if classe == 0:
#                 #     classe = 'bee'
#                 # elif classe == 1:
#                 #     classe = 'beetle'
#                 # elif classe == 2:
#                 #     classe = 'butterfly'
#                 # elif classe == 3:
#                 #     classe = 'fly'
#                 # elif classe == 4:
#                 #     classe = 'other pollinator'
#                 objet = [name_file , 1, h, w, x, y , classe]
                
#                 annot_1img.append(objet)
#                 ligne = file_txt.readline()
            
#             file_txt.close()

#         # trouver le index où le array imgs possède de la même nom name_file
#         index = 0
                
#         while imgs[index][0] != name_file:
#                 index += 1
                
#         labels[index] = annot_1img
#         print(f"{index} : {labels[index]}")
#         print()
            
#     return labels


# def lire_jsons (folder_json : str , imgs): 
#     lenth = len(imgs)
#     labels = np.zeros(lenth)
#     labels = np.ndarray.tolist(labels)

#     for file in glob.glob(folder_json + "*.json") :
#         annot_1img = []
        
#         with open(file , 'r') as json:
#             name_file = file.split("\\")[-1].rsplit(".",1)[0]
#             print(name_file)
#             data_json = js.load(json)
#             annotations = data_json["annotations"].copy()
#             width_img = data_json["item"]["slots"][0]["width"]
#             height_img = data_json["item"]["slots"][0]["height"]
#             if annotations != []:
#                 for annot in annotations:
#                     bool = 1
#                     h , w , x , y = annot["bounding_box"]["h"], annot["bounding_box"]["w"], annot["bounding_box"]["x"], annot["bounding_box"]["y"]
#                     h, w, x, y = int(h), int(w), int(x), int(y)
#                     h = h/height_img
#                     w = w/width_img
#                     x = x/width_img
#                     y = y/height_img
#                     classe = annot["name"]
#                     annot_1img.append([name_file, bool, h, w, x, y, classe ]) #ici, annot name est le nom de pollinisateur.
                    
#             index = 0
                
#             while imgs[index][0] != name_file:
#                     index += 1

#             print(f"{index} : {annot_1img}")
#             print()
#             labels[index] = annot_1img
#             json.close()     


def trouver_index(labels, img_name):
    index = -1
    for i in range(len(labels)):
        if labels[i][0][0] == img_name:
            index = i
    return index


def load_data_detection(img_path : str, labels_path : str, scale_resize = 0.5):

    # Lire le fichier json pour récupérer les labels (0: bee, 1: beetle, 2: butterfly, 3: fly, 4: other pollinator), et récupérer les images. x est la list dont les élément est matrice d'images. y est un list où chaque élément contient les label d'un images.
    
    # Obtenir les labels (y)
    t_debut = time.time()
    labels = []
    for file in glob.glob(labels_path + "*.txt"):
        annot_1_img = []
        nb_ligne = 0
        with open(file , 'r') as file_txt:
            ligne = file_txt.readline()
            name_file = file.split("\\")[-1].rsplit(".",1)[0]
            #bool = name_file != 'WSCT1464' and name_file != 'WSCT1334'
            while ligne != "" : # Si on arrive à la dernière ligne
                classe, x, y, w, h = ligne.split(" ")
                h, w, x, y = float(h), float(w), float(x), float(y)
                classe = int(classe)
                objet = [name_file , 1, x, y, w, h, classe]
                annot_1_img.append(objet)
                ligne = file_txt.readline()
                nb_ligne += 1
            file_txt.close()
        if nb_ligne != 0:
            labels.append(annot_1_img)


    imgs = np.zeros(len(labels))
    imgs = np.ndarray.tolist(imgs)
    for image in glob.glob(img_path + "*"):
        img_name = image.split("\\")[-1].rsplit(".",1)[0]
        index = trouver_index(labels, img_name) # si index == -1, il n'y a pas d'annotation correspondante

        if index != -1:
            img = Image.open(image, 'r')
            img = img.convert('RGB')
            # img = cv2.imread(image, cv2.IMREAD_COLOR)
            shape_img_init = np.shape(img)[0:2]
            img_padding_resized, rapport = padding_resize(img, shape_img_init)
            shape_img = np.shape(img_padding_resized)
            # modifier les annotations après le padding
            for objet in range(len(labels[index])):
              labels[index][objet][2] = labels[index][objet][2]*rapport[1]   # nouvelle x, nouvelle annotation dans les images avec "padding"
              labels[index][objet][3] = labels[index][objet][3]*rapport[0]   # nouvelle y
              labels[index][objet][4] = labels[index][objet][4]*rapport[1]   # nouvelle w
              labels[index][objet][5] = labels[index][objet][5]*rapport[0]   # nouvelle h

            img_mat = np.array(img_padding_resized)
            img_mat = cv2.normalize(img_mat, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
            imgs[index] = [img_name, img_mat]

    print(img_mat[0])
    print(f"durée toute la fonction est {time.time()-t_debut}")
    print( f" lenth imgs is {len(imgs)}, lenth labels is {len(labels)}") 

    return imgs, labels


# Chemin vers la base de données
img_path = "C:\\Users\\juyin\\.darwin\\datasets\\ue-inrae-toulouse\\pollinator\\images\\"
# labels_path = "C:\\Users\\juyin\\.darwin\\datasets\\ue-inrae-toulouse\\pollinator\\releases\\dyolo2-0\\annotations\\"
labels_path = "C:\\Users\\juyin\\Downloads\\polloyolo\\" # en fait il manque 8 fichier txt pour les images respectivesdans ce répertoire. Leurs labels dans array labels sont remplacés par 0.0.
x, y = load_data_detection(img_path, labels_path)

# __________________________________________________________________________________________________________________________________


# In[]
# Garder uniquement les images (et non les noms)
x_img = copy.deepcopy(x)
for img in range(len(y)):
  x_img[img].pop(0)
x_img = np.squeeze(x_img)
  

# __________________________________________________________________________________________________________________________________
# In[ ]:
## Cellule de tests

nombre_confondu = 0
no = 0

for lab in y:
    if lab == [] or lab == 0.:
        no += 1

for i in range(len(x)):
    
    if x[i][0] != y[i][0][0]:
        nombre_confondu += 1
print(no)
print(nombre_confondu)

# __________________________________________________________________________________________________________________________________

# In[40]:
# ### Affichage des données
# 
# Le code ci-dessous vous permettra d'afficher les images ainsi que leurs boîtes englobantes associées. On peut spécifier l'id d'une image en particulier ou, si l'on en spécifie pas, visualiser une image aléatoire.

from scipy.special import softmax
import matplotlib.pyplot as plt

def print_data_detection(x, y, id=None, classes=CLASS_LABELS, image_size=IMAGE_SIZE):
  num_img = -1
  if id==None:
    # Tirage aléatoire d'une image dans la base
    num_img = np.random.randint(x.shape[0])
  else:
    num_img = id
  print(id)
 
  img = x[num_img][1]
    #img = np.array(img_se, dtype = 'float64')
  shape = np.shape(img)
  
  boxes = np.array(y[num_img])

  print(boxes[0][0])
  boxes = boxes[:,1:]
  boxes = np.ndarray.tolist(boxes)
  print(type(boxes))
  
  colors = ["blue", "yellow", "red", "orange", "pink"] # Différentes couleurs pour les différentes classes

  # Affichage de l'image
  plt.imshow(img)
  
  for box in boxes:
    # print(box)
    
    if box[0] != 0:
    # Détermination de la classe
      class_id = int(box[5])

      # Détermination des extrema de la boîte englobante
      #l'ordre de l'enregistrement est  existence, x, y, w, h, label
      p_x = [(float(box[1])-float(box[3])/2)*shape[1], (float(box[1])+float(box[3])/2)*shape[1]]
      p_y = [(float(box[2])-float(box[4])/2)*shape[0], (float(box[2])+float(box[4])/2)*shape[0]]

      # Affichage de la boîte englobante, dans la bonne couleur
      plt.plot([p_x[0], p_x[0]],p_y,color=colors[class_id])
      plt.plot([p_x[1], p_x[1]],p_y,color=colors[class_id])
      plt.plot(p_x,[p_y[0],p_y[0]],color=colors[class_id])
      plt.plot(p_x,[p_y[1],p_y[1]],color=colors[class_id], label=classes[class_id])

    else:
      class_id = None

  plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")
  plt.axis('off')
  plt.show()  

# Affichage d'une image aléatoire
random_liste = np.random.randint(0, len(x), 10)
for index in random_liste:
  print_data_detection(x, y, id=index)

print_data_detection(x, y, id=200)

# __________________________________________________________________________________________________________________________________


# In[ ]:
# Format Yolo


def set_box_for_yolo(x, y, num_classes=NB_CLASSES, nb_cell_per_dim = CELL_PER_DIM):

  y_YOLO = np.zeros((len(y), nb_cell_per_dim, nb_cell_per_dim, 1 + 4 + num_classes))

  for i in range(len(y)):
    
    for box in y[i]:
      # Récupérer l'image correspndante au y et sa taille
      img_shap = np.shape(x[i])
      
      lenth_cell_x = int(img_shap[1] / nb_cell_per_dim)
      lenth_cell_y = int(img_shap[0] / nb_cell_per_dim)
      # print(f"largeur et hauteur d'un cellule est {lenth_cell_x}, {lenth_cell_y}")

      # Coordonnées du centre de la boîte englobante dans le repère image 
      # box : [nom d'image, boolean d'existance , x , y, w , h , classe]
      cx, cy = int(box[2] * img_shap[1]), int(box[3] * img_shap[0])
      # Détermination des indices de la cellule dans laquelle tombe le centre
      ind_x, ind_y = int(cx // lenth_cell_x), int(cy // lenth_cell_y)
      # YOLO : "The (x, y) coordinates represent the center of the box relative to the bounds of the grid cell."
      # On va donc calculer les coordonnées du centre relativement à la cellule dans laquelle il se situe
      y_YOLO[i, ind_x, ind_y, 1] = (cx - ind_x * lenth_cell_x) / lenth_cell_x
      y_YOLO[i, ind_x, ind_y, 2] = (cy - ind_y * lenth_cell_y) / lenth_cell_y
      # Largeur et hauteur de boîte
      y_YOLO[i, ind_x, ind_y, 3] = box[4]
      y_YOLO[i, ind_x, ind_y, 4] = box[5]

      # Indice de confiance de la boîte englobante pour la cellule correspondante
      y_YOLO[i, ind_x, ind_y, 0] = box[1]
      # On range les probabilités de classe à la fin du vecteur ([ Présence ; cx ; cy ; w ; h ; CLASSES])
      classes = to_categorical([box[6]], num_classes=num_classes)
      

      y_YOLO[i, ind_x, ind_y, 5:] = classes[0][0],classes[0][1],classes[0][2],classes[0][3],classes[0][4]

  return y_YOLO



# Si mode = 'pred', il s'agit d'une prédiction du réseau, il faut alors utiliser la fonction sigmoide
# pour obtenir la présence prédite, et la fonction softmax pour obtenir les probabilités de classe
def get_box_from_yolo(y_YOLO, imgs, mode=None, confidence_threshold=0.5, num_classes=NB_CLASSES, nb_cell_per_dim = CELL_PER_DIM):
  
  y = []

  print(len(imgs))

  for i in range(y_YOLO.shape[0]):
    boxes = []
    # Récupérer l'image correspndante au y et sa taille
    img_shap = np.shape(imgs[i])
    
    lenth_cell_x = int(img_shap[1] / nb_cell_per_dim)
    lenth_cell_y = int(img_shap[0] / nb_cell_per_dim)
    for ind_x in range(0, nb_cell_per_dim):
      for ind_y in range(0, nb_cell_per_dim):
        if mode == 'pred':
          presence = expit(y_YOLO[i, ind_x, ind_y, 0])
          classes_probabilities = softmax(y_YOLO[i, ind_x, ind_y, 5:])
          # coords = expit(y_YOLO[i, ind_x, ind_y, 1:5])
        else:
          presence = y_YOLO[i, ind_x, ind_y, 0]
          classes_probabilities = y_YOLO[i, ind_x, ind_y, 5:]

        coords = y_YOLO[i, ind_x, ind_y, 1:5]
        if presence > confidence_threshold:

          box = []
          box.append((coords[0] * lenth_cell_y + ind_x * lenth_cell_y) / img_shap[1])
          box.append((coords[1] * lenth_cell_x + ind_y * lenth_cell_x) / img_shap[0])
          box.append(coords[2])
          box.append(coords[3])
          box.append(np.argmax(y_YOLO[i, ind_x, ind_y, 5:]))
          box.append(presence)
          boxes.append(box)

    y.append(boxes)
  print(len(y))

  return y

# On s'assure de pouvoir passer d'une représentation à l'autre sans altérer les données
# print(get_box_from_yolo(set_box_for_yolo(x, y), x, mode='pred'))


# ### Augmentation de données

# Il nous faut une version récente de la librairie Albumentations pour pouvoir profiter des fonctionnalités lies aux boîtes englobantes.

# __________________________________________________________________________________________________________________________________


# In[7]:


# !pip uninstall opencv-python-headless==4.5.5.62
# !pip install opencv-python-headless==4.1.2.30
# !pip install -q -U albumentations
# !echo "$(pip freeze | grep albumentations) is successfully installed"


# Dans la cellule ci-dessous, il vous faudra intégrer les augmentations que vous aurez choisi. **Attention, ne faites cette partie que dans un second temps, lorsque vous aurez une première version du réseau qui fonctionnera !**
# 
# Aidez-vous de [cette page](https://albumentations.ai/docs/getting_started/transforms_and_targets/) pour déterminer des augmentations qui peuvent fonctionner.


# __________________________________________________________________________________________________________________________________

# In[ ]:


from albumentations import (Compose, HorizontalFlip)
import albumentations as A

AUGMENTATIONS_TRAIN = Compose([
    #### A COMPLETER, MAIS SEULEMENT LORSQUE VOUS AVEZ UN RESEAU QUI (SUR-)APPREND !
    HorizontalFlip(p=0.5),
    A.RandomBrightnessContrast(p=0.2),
], bbox_params=A.BboxParams(format='yolo'))


# L'objet Sequence défini ci-dessous nous permettra la mise en batch de nos données. On est obligé d'avoir recours à cette solution (plutôt qu'un ImageDataGenerator comme lors du TP3) car ici les augmentations à appliquer altèrent également les labels, ce qui n'est pas supporté par un ImageDataGenerator.

# __________________________________________________________________________________________________________________________________

# In[ ]:


class YOLOSequence(Sequence):
    # Initialisation de la séquence avec différents paramètres
    # def __init__(self, x_set, y_set, batch_size, augmentations):
    #     self.x, self.y = x_set, y_set
    #     self.batch_size = batch_size
    #     self.augment = augmentations
    #     self.indices1 = np.arange(x_set.shape[0], dtype='int')
    #     np.random.shuffle(self.indices1) # Les indices permettent d'accéder
    #     # aux données et sont randomisés à chaque epoch pour varier la composition
    #     # des batches au cours de l'entraînement

    def __init__(self, x_set, y_set, batch_size):

        print("hey !")

        self.x, self.y = x_set, y_set
        self.batch_size = batch_size
        self.indices1 = np.arange(len(x_set), dtype='int')
        np.random.shuffle(self.indices1)

    # Fonction calculant le nombre de pas de descente du gradient par epoch
    def __len__(self):
        return int(np.ceil(len(self.x) / float(self.batch_size)))


    # Il y a des problèmes d'arrondi dans les conversions de boîtes englobantes
    # internes à la librairie Albumentations
    # Pour les contourner, si les boîtes sont trop proches des bords, on les érode un peu
    def erode_bounding_box(self, box, epsilon = 0.02):
        eroded_box = []

        xmin = max(box[0] - box[2]/2, epsilon)
        ymin = max(box[1] - box[3]/2, epsilon)
        xmax = min(box[0] + box[2]/2, 1-epsilon)
        ymax = min(box[1] + box[3]/2, 1-epsilon)

        cx = xmin + (xmax - xmin)/2
        cy = ymin + (ymax - ymin)/2
        width = xmax - xmin
        height = ymax - ymin

        eroded_box = [cx, cy, width, height, box[4]]
        return eroded_box

    # Application de l'augmentation de données à chaque image du batch et aux
    # boîtes englobantes associées
    def apply_augmentation(self, bx, by):

        batch_x = np.zeros((bx.shape[0], IMAGE_SIZE, IMAGE_SIZE, 3))
        batch_y = []

        # Pour chaque image du batch
        for i in range(len(bx)):
            boxes = []
            # Erosion des boîtes englobantes
            for box in by[i]:
              boxes.append(self.erode_bounding_box(box))

            # Application de l'augmentation à l'image et aux boîtes englobantes
            transformed = self.augment(image=bx[i].astype('float32'), bboxes=boxes)
            batch_x[i] = transformed['image']
            batch_y_transformed = transformed['bboxes']
            batch_y.append(batch_y_transformed)

        return batch_x, batch_y

    # Fonction appelée à chaque nouveau batch : sélection et augmentation des données
    def __getitem__(self, idx):
        # Sélection des indices des données du prochain batch
        batch_indices = self.indices1[idx * self.batch_size:(idx + 1) * self.batch_size]
        print("un batch en voyage")
        # Récupération des données puis des labels du batch
         
        # Garder uniquement les images (et non les noms)
        # set_x = copy.deepcopy(self.x)
        # for img in range(len(self.y)):
        #   set_x[img].pop(0)

        set_x = copy.deepcopy(self.x)
        print(f"shape x est {np.shape(set_x)}")

        batch_x =[] #initialisaition de batch image
        for indexx in batch_indices:
           batch_x.append(set_x[indexx])
           
        print(f"shape batch_x est {np.shape(batch_x)}")
        # print(f"batch x est {batch_x}")
        batch_boxes = [self.y[item] for item in list(batch_indices)]
        print(f"shape batch boxes est {np.shape(batch_boxes)}")
        #print(f"batch boxes courant est {batch_boxes}")
        # Application de l'augmentation de données
        # batch_x_aug, batch_boxes_aug = self.apply_augmentation(batch_x, batch_boxes)
        batch_x_aug, _ = batch_x, batch_boxes

        # Préparation des données pour le réseau :
        # Normalisation des entrées
        # batch_x_aug = batch_x_aug/255

        # Passage des sorties au format YOLO
        # batch_y_YOLO = set_box_for_yolo(batch_x_aug, batch_boxes)
        print("get_item done")
        return np.array(batch_x_aug), batch_boxes

    # Fonction appelée à la fin d'un epoch ; on randomise les indices d'accès aux données
    def on_epoch_end(self):
        np.random.shuffle(self.indices1)

# __________________________________________________________________________________________________________________________________

# In[ ]:

# Instanciation d'une Sequence
# train_gen = YOLOSequence(x[:1], y[:1], 1, augmentations=AUGMENTATIONS_TRAIN)

# train_gen = YOLOSequence(x, y, 1)

# Pour tester la séquence, nous sélectionnons les éléments du premier batch et les affichons
# batch_x, batch_y = train_gen.__getitem__(0)
# print(batch_x.shape)

#print_data_detection(batch_x, get_box_from_yolo(batch_y, batch_x))


# ## Implémentation de YOLO

# ### Modèle

# 
# <center> <img src="https://drive.google.com/uc?id=1_wXc_gTIAr37STaxu3chq1EEjVSKv6a5" width=500></center>
# <caption><center> Illustration de la couche de sortie de YOLO. </center></caption>
# 
# Le modèle que je vous propose ci-dessous n'est qu'une possibilité parmi beaucoup d'autres.
# A vous de compléter la dernière couche pour avoir une sortie de la bonne dimension.
# 
# **Remarque importante** : comme le tenseur de sortie de YOLO est un peu complexe à manipuler, on choisit ici de regrouper l'ensemble des prédictions dans une seule et même sortie, ce qui nous oblige à utiliser la même fonction d'activation pour toutes nos sorties. On utilisera donc l'activation **linéaire** pour toutes ces sorties. On appliquera les fonctions sigmoïde et softmax pour les sorties "présence" et "probablités de classe" directement dans la fonction de coût !

# __________________________________________________________________________________________________________________________________

# In[ ]:

def YOLOV1(input_shape=(448, 448, 3)):
  # input_layer = Input(shape=input_shape, dtype='int32')
  input_layer = Input(shape=input_shape)

  conv1 = Conv2D(64, 7, activation = 'relu', strides=(1, 1), kernel_initializer = 'he_normal')(input_layer)
  pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

  conv2 = Conv2D(192, 3, activation = 'relu', kernel_initializer = 'he_normal')(pool1)
  pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

  conv3 = Conv2D(128, 1, activation = 'relu')(pool2)
  conv3 = Conv2D(256, 3, activation = 'relu')(conv3)
  conv3 = Conv2D(256, 1, activation = 'relu')(conv3)
  conv3 = Conv2D(512, 3, activation = 'relu')(conv3)
  pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

  conv4 = Conv2D(256, 1, activation = 'relu')(pool3)
  conv4 = Conv2D(512, 3, activation = 'relu')(conv4)
  conv4 = Conv2D(256, 1, activation = 'relu')(conv4)
  conv4 = Conv2D(512, 3, activation = 'relu')(conv4)
  conv4 = Conv2D(256, 1, activation = 'relu')(conv4)
  conv4 = Conv2D(512, 3, activation = 'relu')(conv4)
  conv4 = Conv2D(256, 1, activation = 'relu')(conv4)
  conv4 = Conv2D(512, 3, activation = 'relu')(conv4)
  conv4 = Conv2D(512, 1, activation = 'relu')(conv4)
  conv4 = Conv2D(1024, 3, activation = 'relu')(conv4)
  pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

  conv5 = Conv2D(512, 1, activation = 'relu')(pool4)
  conv5 = Conv2D(1024, 3, activation = 'relu')(conv5)
  conv5 = Conv2D(512, 1, activation = 'relu')(conv5)
  conv5 = Conv2D(1024, 3, activation = 'relu')(conv5)
  conv5 = Conv2D(1024, 3, activation = 'relu')(conv5)  
  

  conv6 = Conv2D(1024, 3, activation = 'relu')(conv5)
  conv6 = Conv2D(1024, 3, activation = 'relu')(conv6)

  # Connected layers
  flatten = Flatten()(conv6)
  fc1 = Dense(4096, activation = 'relu')(flatten)
  fc1 = Dense(7*7*30, activation = 'linear')(fc1)

  output = Reshape((7,7,30))(fc1)

  model = Model(input_layer, output)

  return model

# __________________________________________________________________________________________________________________________________

# In[ ]:


model = YOLOV1()
model.summary()


# ### Fonction de coût

# <center> <img src="https://drive.google.com/uc?id=1Fbt_Wh_BqZj8Pwt3-04325ItCkQp5G9X" style="width:500;height:300px;"></center>
# <caption><center> Détail de la fonction de perte définie dans l'article YOLO v1 </center></caption>
# 
# Nous arrivons maintenant à la partie délicate de l'implémentation de YOLO : la définition de la fonction de coût à utiliser.
# 
# Comme nous l'avons vu dans le TP4, lorsque l'on écrit une fonction de coût personnalisée en Keras, il est nécessaire d'utiliser uniquement les fonctions présentes sur la page suivante :
# https://keras.rstudio.com/articles/backend.html
# 
# En effet cette fonction de coût qui sera appelée pendant l'entraînement traitera des tenseurs, et non des tableau *numpy*. On doit donc utiliser la librairie Tensorflow qui permet de manipuler les tenseurs.
# 
# Une partie essentielle de la fonction est déjà écrite : celle qui permet de séparer les données des cellules dites "vide" (la vérité terrain ne contient pas de boîte englobante) des "non vides".
# 
# Le détail de la fonction de coût est indiqué ci-dessus : dans l'article $\lambda_{\text{coord}} = 5$ et $\lambda_{\text{noobj}} = 0.5$. Les $x_i$, $y_i$, $w_i$, $h_i$ correspondent aux coordonnées d'une boîte englobante, $C_i$ correspond à la probabilité de présence d'un objet dans la cellule, et les $p_i(c)$ sont les probabilités de classe.
# 
# A vous de compléter l'expression des sous-fonctions de la fonction de coût (les fonctions *K.sum*, *K.square*, *K.sigmoid* et *K.softmax* devraient vous suffire !). **N'oubliez pas d'appliquer une sigmoïde aux présences ($C_i$) et une softmax aux probabilités de classe $p_i$) !!**
# 
# **NB : cette implémentation de la fonction de coût est très simplifiée et prend en compte le fait qu'il n'y a qu'une seule boîte englobante par cellule.**

# __________________________________________________________________________________________________________________________________

# In[ ]:


from keras import backend as K

# Définition de la fonction de perte YOLO
def YOLOss(lambda_coord, lambda_noobj, batch_size):

    # Partie "verte" : sous-partie concernant l'indice de confiance et les
    # probabilités de classe dans le cas où une boîte est présente dans la cellule
    def box_loss(y_true, y_pred):
      print(f"shape of y_true is {np.shape(y_true)}, in box_loss")

      sum1 = K.sum(K.square((y_true[:,0]) - K.sigmoid(y_pred[:,0])))
      sum2 = K.sum(K.square((y_true[:,5:]) - K.softmax(y_pred[:,5:])))
      return sum1 + sum2

    # Partie "bleue" : sous-partie concernant les coordonnées de boîte englobante
    # dans le cas où une boîte est présente dans la cellule
    def coord_loss(y_true, y_pred):
      print(f"y_true is {y_true}, function coord_loss")
      print(f"shape of y_true is {np.shape(y_true)}")

      x_chap = y_pred[:,1]
      y_chap = y_pred[:,2]
      w_chap = y_pred[:,3]
      h_chap = y_pred[:,4]

      x = y_true[:,1]
      y = y_true[:,2]
      w = y_true[:,3]
      h = y_true[:,4]

      sum1 = K.sum(K.square(x-x_chap) + K.square(y-y_chap))
      sum2 = K.sum(K.square(K.sqrt(w)-K.sqrt(w_chap)) + K.square(K.sqrt(h)-K.sqrt(h_chap)))
      return sum1 + sum2


    # Partie "rouge" : sous-partie concernant l'indice de confiance
    # dans le cas où aucune boîte n'est présente dans la cellule
    def nobox_loss(y_true, y_pred):
      return K.sum(K.square((y_true[:,0]) - K.sigmoid(y_pred[:,0])))


    def YOLO_loss(y_true, y_pred):

      # On commence par reshape les tenseurs de bs x S x S x (5B+C) à (bsxSxS) x (5B+C)
      y_true = K.reshape(y_true, shape=(-1, 9))
      y_pred = K.reshape(y_pred, shape=(-1, 9))

      # On cherche (dans les labels y_true) les indices des cellules pour lesquelles au moins la première boîte englobante est présente
      not_empty = K.greater_equal(y_true[:, 0], 1)
      indices = K.arange(0, K.shape(y_true)[0])
      indices_notempty_cells = indices[not_empty]

      empty = K.less_equal(y_true[:, 0], 0)
      indices_empty_cells = indices[empty]

      # On sépare les cellules de y_true et y_pred avec ou sans boîte englobante
      y_true_notempty = K.gather(y_true, indices_notempty_cells)
      y_pred_notempty = K.gather(y_pred, indices_notempty_cells)

      y_true_empty = K.gather(y_true, indices_empty_cells)
      y_pred_empty = K.gather(y_pred, indices_empty_cells)

      return (box_loss(y_true_notempty, y_pred_notempty) + lambda_coord*coord_loss(y_true_notempty, y_pred_notempty) + lambda_noobj*nobox_loss(y_true_empty, y_pred_empty))/batch_size


    # Return a function
    return YOLO_loss


# ### Apprentissage

# Comme d'habitude, on sépare nos données en plusieurs ensembles (ici apprentissage et validation suffiront).

# __________________________________________________________________________________________________________________________________

# In[ ]:
#arranger le y en format yolo
y_yolo = set_box_for_yolo(x_img, y)

# Séparation des données en ensemble de validation et d'apprentissage
indices = np.arange(np.shape(x_img)[0], dtype='int')

np.random.seed(1)
np.random.shuffle(indices)


limite = int(len(indices)*0.8)

# Créer x_train avec les indices de 0 à limite
x_train = [x_img[item] for item in list(indices[:limite])]
y_train = [y_yolo[item] for item in list(indices[:limite])]

# Créer x_val avec les indices de limite à la fin
x_val = [x_img[item] for item in list(indices[limite:])]
y_val = [y_yolo[item] for item in list(indices[limite:])]


# Prenez le temps de tester votre modèle et votre fonction de coût, ainsi que vos réglages d'hyperparamètres, en sur-apprenant sur une image d'abord, puis sur un batch d'images. Entraînez votre réseau et visualisez ses prédictions sur les données d'entraînement, puis de validation, pour obtenir une intuition sur les valeurs de *loss* permettant d'obtenir des résultats "acceptables".

# __________________________________________________________________________________________________________________________________
# In[ ]:
print(y_train[0])
print(np.shape(y_train))

# __________________________________________________________________________________________________________________________________


# In[ ]:

batch_size = 16
model = YOLOV1()
### A COMPLETER
learning_rate = 3e-4
opt = Adam(learning_rate=learning_rate)

# Instanciation de la séquence pour préparer les données et, plus tard,
# train_gen = YOLOSequence(x_train, y_train, batch_size, augmentations=AUGMENTATIONS_TRAIN)
train_gen = YOLOSequence(x_train, y_train, batch_size)

print("y_train", np.shape(y_train))

# Comme l'entraînement est instable, on déclenche une sauvegarde du modèle à chaque fois que
# la perte de validation atteint un nouveau minimum
model_saver = ModelCheckpoint('tmp/best_weights', monitor='val_loss', verbose=1, save_weights_only=True, save_best_only=True, mode='min')

loss=[YOLOss(5, 0.5, batch_size)]




# ________________________________________________________________________________________________________________________________
# In[] : 

model.compile(loss=loss,
              optimizer=opt)

history = model.fit(train_gen,
              epochs=150,
              batch_size=batch_size,
              validation_data=(x_val, y_val),
              callbacks=[model_saver])


# ### Test et affichage des résultats

# #### Test de la version à la fin de l'entrainement

# **Sur l'ensemble d'apprentissage**

# __________________________________________________________________________________________________________________________________

# In[ ]:


y_pred = model.predict(x_train)
y_pred_YOLO = get_box_from_yolo(y_pred, confidence_threshold=0.5, mode='pred')
print_data_detection(x_train, y_pred_YOLO)


# Une fois les prédictions effectuées, vous pouvez pour aller plus rapidement uniquement relancer l'affichae aléatoire d'un seul résultat.

# __________________________________________________________________________________________________________________________________

# In[ ]:


print_data_detection(x_train, y_pred_YOLO)


# **Sur l'ensemble de validation**

# __________________________________________________________________________________________________________________________________

# In[ ]:


y_pred = model.predict(x_val/255)
y_pred_YOLO = get_box_from_yolo(y_pred, confidence_threshold=0.5, mode='pred')
print_data_detection(x_val, y_pred_YOLO)

# __________________________________________________________________________________________________________________________________
# In[ ]:


print_data_detection(x_val, y_pred_YOLO)


# #### Test de la meilleure version sauvegardée

# La cellule ci-dessous vous permettra de charger les poids sauvegardés lorsque la meilleur performance a été atteinte sur l'ensemble de validation pendant l'entraînement.

# __________________________________________________________________________________________________________________________________
# In[ ]:


model.load_weights('tmp/best_weights')


# **Sur l'ensemble d'apprentissage**

# __________________________________________________________________________________________________________________________________
# In[ ]:


y_pred = model.predict(x_train/255)
y_pred_YOLO = get_box_from_yolo(y_pred, confidence_threshold=0.5, mode='pred')
print_data_detection(x_train, y_pred_YOLO)

# __________________________________________________________________________________________________________________________________
# In[ ]:


print_data_detection(x_train, y_pred_YOLO)


# **Sur l'ensemble de validation**

# __________________________________________________________________________________________________________________________________
# In[ ]:


y_pred = model.predict(x_val/255)
y_pred_YOLO = get_box_from_yolo(y_pred, confidence_threshold=0.3, mode='pred')
print_data_detection(x_val, y_pred_YOLO)

# __________________________________________________________________________________________________________________________________
# In[ ]:


print_data_detection(x_val, y_pred_YOLO)

# __________________________________________________________________________________________________________________________________